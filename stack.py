# stack data structure
class Stack():

    # Constructor; set initial stack to an empty list
    def __init__(self):
        self.items = []

    # Add item to stack
    def push(self, item):
        self.items.append(item)

    # Remove an item from the stack
    def pop(self):
        return self.items.pop()

    # Check if stack is empty
    def is_empty(self):
        if not self.items:
            print("The stack is empty")
            return True
        else:
            print("The stack is not empty")
            return False

    # Function to return the top from stack without removing it
    def peek(self):
        if not self.is_empty():
            print("The top of the stack is " + str(self.items[-1]))
            return self.items[-1]
        else:
            return None

    def get_stack(self):
        return self.items

# test if is_empty works
s = Stack()
s.is_empty()

# test if push works
s.push(1)
s.push(2)
s.push(3)
# test if is_empty works when it has items in the stack
s.is_empty()
# test get_stack
print(s.get_stack())

# test pop
s.pop()
print(s.get_stack())
# test peek
s.peek()

s.pop()
s.pop()
# test peek when stack is empty
s.peek()